package com.polontech.confluence.pingservice.ao;

import net.java.ao.Entity;

public interface AffectedSpaces extends Entity {

    String getAffectedSpaceKey();

    void setAffectedSpaceKey(String key);
}
