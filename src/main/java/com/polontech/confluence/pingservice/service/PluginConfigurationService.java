package com.polontech.confluence.pingservice.service;

import com.polontech.confluence.pingservice.servlet.JsonDataObject;

import java.util.Map;

public interface PluginConfigurationService {

    boolean updateConfigurationFromJSON(JsonDataObject jsonDataObject);

    Map<String, Object> getConfiguration();
}
